const db = require('./baza.js')

const express = require('express')
const bcrypt = require('bcrypt')
const session = require("express-session")
const bodyParser = require('body-parser')
const fs = require('fs')
const app = express()
const path = require('path')
app.use(bodyParser.json())

app.set("view engine", "pug");
app.set("views", path.join(__dirname, "/public/views"))

app.use(session({
    secret: 'neka tajna sifra',
    resave: true,
    saveUninitialized: true
    }))

db.sequelize.sync({force:true}).then(async (data)=>{
    console.log("Sync successful!")
    await addTestData()
}).catch((err)=>{
    console.log("Sync error!")
})

const addTestData = async function(){
    await db.student.bulkCreate([
        {
            id: 1, ime: "Neko Nekic", index: 12345
        },
        { 
            id: 2, ime: "Drugi Neko", index: 12346
        },
        { 
            id: 3, ime: "Treci Neko", index: 12347
        }
        ])

    await db.nastavnik.bulkCreate([
            {
                id: 1, username: "Naida", password_hash: "$2a$10$h8x1ClkmKuz6dHF1xKpbKOU2kLkIgT2WGtGjEs20b5EIaiJc7jqti"
            },
            { 
                id: 2, username: "Adem", password_hash: "$2a$10$PkGKas/8ENNj9YTnOfJdIeWodl81ZxLT6LyYhiVSl5XhK1YxphI5e"
            }
            ])

    await db.predmet.bulkCreate([
        {
            id:1,
            naziv: "Razvoj mobilnih aplikacija",
            brojPredavanjaSedmicno: 2,
            brojVjezbiSedmicno: 2,
            NastavnikId: 1
        },
        { 
            id:2,
            naziv: "Osnove informacionih istrazivanja",
            brojPredavanjaSedmicno: 2,
            brojVjezbiSedmicno: 2,
            NastavnikId: 1
        },
        { 
            id:3,
            naziv: "Web tehnologije",
            brojPredavanjaSedmicno: 2,
            brojVjezbiSedmicno: 2,
            NastavnikId:2
        }
        ])

    await db.prisustvo.bulkCreate([
        {
            id: 1,
            sedmica: 1,
            predavanja: 1,
            vjezbe: 1,
            index: 12345,
            PredmetId: 1
        },
        {
            id: 2,
            sedmica: 1,
            predavanja: 2,
            vjezbe: 1,
            index: 12346,
            PredmetId: 1
        },
        {
            id: 3,
            sedmica: 1,
            predavanja: 1,
            vjezbe: 2,
            index: 12347,
            PredmetId: 1
        },
        {
            id: 4,
            sedmica: 2,
            predavanja: 0,
            vjezbe: 0,
            index: 12345,
            PredmetId: 1
        },
        {
            id: 5,
            sedmica: 2,
            predavanja: 2,
            vjezbe: 1,
            index: 12347,
            PredmetId: 1
        },
        {
            id: 6,
            sedmica: 2,
            predavanja: 2,
            vjezbe: 1,
            index: 12346,
            PredmetId: 1
        },
        {
            id: 7,
            sedmica: 3,
            predavanja: 1,
            vjezbe: 0,
            index: 12345,
            PredmetId: 1
        },
        {
            id: 8,
            sedmica: 3,
            predavanja: 0,
            vjezbe: 1,
            index: 12346,
            PredmetId: 1
        },
        {
            id: 9,
            sedmica: 3,
            predavanja: 2,
            vjezbe: 1,
            index: 12347,
            PredmetId: 1
        },
        {
            id: 10,
            sedmica: 4,
            predavanja: 2,
            vjezbe: 1,
            index: 12345,
            PredmetId: 1
        },
        {
            id: 11,
            sedmica: 4,
            predavanja: 2,
            vjezbe: 0,
            index: 12347,
            PredmetId: 1
        },
        //drugi predmet
        {
            id: 12,
            sedmica: 1,
            predavanja: 0,
            vjezbe: 2,
            index: 12345,
            PredmetId: 2
            },
            {
            id: 13,
            sedmica: 2,
            predavanja: 1,
            vjezbe: 0,
            index: 12346,
            PredmetId: 2
            },
            //treci predmet
            {
            id: 14,
            sedmica: 1,
            predavanja: 0,
            vjezbe: 2,
            index: 12345,
            PredmetId: 3
            },
            {
            id: 15,
            sedmica: 1,
            predavanja: 2,
            vjezbe: 1,
            index: 12346,
            PredmetId: 3
            },
            {
            id: 16,
            sedmica: 2,
            predavanja: 1,
            jezbe: 0,
            index: 12345,
            PredmetId: 3
            },
            {
            id: 17,
            sedmica: 2,
            predavanja: 0,
            vjezbe: 1,
            index: 12346,
            PredmetId: 3
            },
            { 
            id: 18,
            sedmica: 3,
            predavanja: 1,
            vjezbe: 0,
            index: 12345,
            PredmetId: 3
            },
            {
            id: 19,
            sedmica: 3,
            predavanja: 0,
            vjezbe: 1,
            index: 12346,
            PredmetId: 3
            }
        ])
    

    await db.Student_Predmet.bulkCreate([
        {
            StudentId:1,
            PredmetId:1
        },
        {
            StudentId:1,
            PredmetId:2
        },
        {
            StudentId:1,
            PredmetId:3
        },
        {
            StudentId:2,
            PredmetId:1
        },
        {
            StudentId:2,
            PredmetId:2
        },
        {
            StudentId:2,
            PredmetId:3
        },
        {
            StudentId:3,
            PredmetId:1
        }
    ])
}
/*BAZA KOMANDE*/
db.sequelize.authenticate().then(()=>{
    console.log("Successful connection!")
}).catch((err)=>{
    console.log("Error connecting to the database!")
})


//_dirname allows us to get the root directory of our project
//u slucaju da ne moze naci neki file, onda ubacuje "/css, /html, /scripts" u put putanje

//_dirname allows us to get the root directory of our project

//to test, run http://localhost:3000/prisustvo.html
//u terminalu kucati node Zadatak2\Z2.js

app.get('/prisustvo.html', (req, res) => {
    res.sendFile(path.join(__dirname,'public','html','prisustvo.html'));
});

app.get('/predmet.html', (req, res) => {
    res.sendFile(path.join(__dirname,'public','html','predmet.html'));
});

app.get('/predmeti.html', (req, res) => {
    if(req.session.predmeti==null){
        res.json({greska:'Nastavnik nije loginovan'})
    }else{
        res.render("predmeti",{predmetiLista:req.session.predmeti})
    }
});

app.get('/prijava.html',(req, res) => {
    res.sendFile(path.join(__dirname,'public','html','prijava.html'));
})

app.get('/predmet/:naziv', (req, res) => {

    db.predmet.findOne({where:{naziv: req.params.naziv}}).then(function(course){
        if(course){
            db.prisustvo.findAll({where:{PredmetId: course.id}}).then(function(prisustvo1){
                var prisustva=prisustvo1.map(o=>o.toJSON())
                
                db.Student_Predmet.findAll({where:{PredmetId:course.id}}).then(async function(studenti){
                    if(studenti){
                        var listaStudenata=[]
                        for(let i=0;i<studenti.length;i++){
                           const s = await db.student.findOne({where:{id:studenti[i].StudentId}})
                           if(s){
                            listaStudenata.push(s)
                           }
                        }
                        var s1=listaStudenata.map(o=>o.toJSON())
                        var objekat={
                            "studenti":s1,
                            "prisustva":prisustva,
                            "predmet":req.params.naziv,
                            "brojPredavanjaSedmicno":course.brojPredavanjaSedmicno,
                            "brojVjezbiSedmicno":course.brojVjezbiSedmicno
                        }
                        res.json(JSON.stringify(objekat))
                    }else{
                        res.json({poruka: 'Neuspješni pronalazak prisustva' })
                    }
                }).catch((err)=>{res.json({poruka: 'Neuspješni pronalazak prisustva' })})
            }).catch((err)=>{res.json({poruka: 'Neuspješni pronalazak prisustva' })})
        }else{
            res.json({poruka: 'Neuspješni pronalazak prisustva' })
        }
    })
})

app.post('/prisustvo/predmet/:naziv/student/:index' ,(req,res) => {
    //inside the body we have {sedmica:N,predavanja:P,vjezbe:V}
    var jsonObj=JSON.parse(JSON.stringify(req.body))


    db.predmet.findOne({where: {naziv: req.params.naziv}}).then(function(foundPredmet){

                db.prisustvo.update({predavanja: jsonObj.predavanja, vjezbe: jsonObj.vjezbe},
                                    {where: {sedmica: jsonObj.sedmica,
                                             index: req.params.index, 
                                             PredmetId: foundPredmet.id}}).then(async function(success){
                    if(success[0]==0){
                        db.prisustvo.create(
                            {
                                sedmica: jsonObj.sedmica,
                                predavanja: jsonObj.predavanja,
                                vjezbe: jsonObj.vjezbe,
                                index: req.params.index,
                                PredmetId: foundPredmet.id
                            }
                        )
                    }
                    db.Student_Predmet.findAll({where:{PredmetId:foundPredmet.id}}).then(async function(studenti){
                        if(studenti){
                            var listaStudenata=[]
                            for(let i=0;i<studenti.length;i++){
                               const s = await db.student.findOne({where:{id:studenti[i].StudentId}})
                               if(s){
                                listaStudenata.push(s)
                               }
                            }

                            var s1=listaStudenata.map(o=>o.toJSON())
                            var newPrisustva = await db.prisustvo.findAll({where: {PredmetId: foundPredmet.id}})
                            var pp=newPrisustva.map(o=>o.toJSON())
                            var objekat={
                                "studenti": s1,
                                "prisustva": pp,
                                "predmet": req.params.naziv,
                                "brojPredavanjaSedmicno": foundPredmet.brojPredavanjaSedmicno,
                                "brojVjezbiSedmicno": foundPredmet.brojVjezbiSedmicno
                            }
                            res.json(JSON.stringify(objekat))
                        }
                    }).catch((err)=>{
                        console.log("Error pri unosu prisustva!")
                        res.json({poruka: 'Neuspješni unos prisustva' })
                    })

                }).catch((err)=>{
                    console.log("Error pri unosu prisustva!")
                    res.json({poruka: 'Neuspješni unos prisustva' })
                })

    }).catch((err)=>{
        console.log("Neuspjesno unosenje prisustva")
        res.json({poruka: 'Neuspješni unos prisustva' })
    })
})

app.post('/login', (req, res) => { 
    var jsonObj=JSON.parse(JSON.stringify(req.body))
    db.nastavnik.findAll({where: {username : jsonObj.username}}).then(async function(nastavnici){
        if(nastavnici.length>0){
            let found=false
            for(let i=0;i<nastavnici.length;i++){
                
                var isPasswordMatched = bcrypt.compareSync(
                    jsonObj.password,
                    nastavnici[i].password_hash
                  )
                if(isPasswordMatched){
                    await db.predmet.findAll({where: {NastavnikId: nastavnici[i].id}}).then(function(courses){
                        if(courses){
                            found=true
                            req.session.predmeti=courses.map(o=>o.naziv)
                        }
                    })
                }

            }
            if(found){
                res.json({poruka: 'Uspješna prijava' })
                return
            }else{
                res.json({poruka: 'Neuspješna prijava' })
                return
            }
        }else{
            res.json({poruka: 'Neuspješna prijava' })
            return
        }
    }).catch((err)=>{console.log("Nesto se desilo kod login-a")})
})  

app.post('/logout', (req,res) => {
    //brise podatke iz sesije
    if(req.session.username!=null){
        req.session.username=null
        req.session.predmeti=null
        res.end(JSON.stringify({poruka: 'Uspješan logout'}))
    }else{
        res.end(JSON.stringify({poruka: 'Neuspješan logout'}))
    }
})

app.use(express.static(path.join(__dirname, 'public')))
app.use('/css', express.static(path.join(__dirname)))
app.use('/html', express.static(path.join(__dirname)))
app.use('/scripts', express.static(path.join(__dirname)))
app.use('/img', express.static(path.join(__dirname)))

let exportt=app.listen(3000, ()=> {
    console.log("Server is running at port 3000...")
});

module.exports = exportt